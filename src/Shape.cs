﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwinGameSDK;
namespace MyGame
{
    class Shape
    {

        //global variables
        private float _x;
        private float _y;
        private int _width;
        private int _height;
        private Color _clr;

        //constructor
        public Shape()
        {
            _x = 0;
            _y = 0;
            _clr = Color.Green;
            _width = 100;
            _height = 100;
        }

        //draw method for drawing the shape
        public void Draw()
        {
            SwinGame.FillRectangle(_clr, _x, _y, _width, _height);
        }
        public bool IsAt(Point2D p)
        {
            return SwinGame.PointInRect(p, _x, _y, _width, _height);
        }

        //properties
        public float X
        {
            get { return _x; }
            set { _x = value; }
        }
        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }
        public Color Color
        {
            get { return _clr; }
            set { _clr = value; }
        }
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }
        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

    }
}
